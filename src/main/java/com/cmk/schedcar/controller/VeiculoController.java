package com.cmk.schedcar.controller;

import com.cmk.schedcar.dto.RetornoVeiculoDTO;
import com.cmk.schedcar.dto.VeiculoDTO;
import com.cmk.schedcar.model.Veiculo;
import com.cmk.schedcar.service.ClienteService;
import com.cmk.schedcar.service.VeiculoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/veiculo")
public class VeiculoController {

    @Autowired
    private VeiculoService veiculoService;

    @Autowired
    private ClienteService clienteService;

    @GetMapping()
    @ResponseBody
    private List<RetornoVeiculoDTO> buscar(){
       List<Veiculo> veiculos = new ArrayList<>();
       veiculoService.findAll().forEach(veiculos::add);
       return veiculos.stream()
               .map(veiculo -> RetornoVeiculoDTO.toRetornoDTO(veiculo))
               .collect(Collectors.toList());

    }

    @PostMapping()
    @ResponseBody
    private RetornoVeiculoDTO salvar(@RequestBody VeiculoDTO veiculo){
        Veiculo novoVeiculo = veiculo.toEntity();
        veiculoService.save(novoVeiculo);
        novoVeiculo.setProprietario(clienteService.findById(novoVeiculo.getIdProprietario()).orElse(null));
        return RetornoVeiculoDTO.toRetornoDTO(novoVeiculo);
    }

    @PutMapping("/atualizar/{id}")
    private ResponseEntity<String> atualizar(@PathVariable Long id, @RequestBody VeiculoDTO veiculoDTO){
        Veiculo veiculo = veiculoService.findById(id).orElse(null);
        if (Objects.isNull(veiculo)){
            return new ResponseEntity<String>("Veículo não encontrado!", HttpStatus.BAD_REQUEST);
        } else {
            veiculo.setWithDTO(veiculoDTO);
            veiculoService.save(veiculo);
            return new ResponseEntity<String>("Veículo atualizado com sucesso!", HttpStatus.OK);
        }

    }

    @DeleteMapping("/remover/{id}")
    private ResponseEntity<String> remover(@PathVariable Long id){
        Veiculo veiculo = veiculoService.findById(id).orElse(null);
        if (Objects.isNull(veiculo)){
            return new ResponseEntity<>("Veículo não encontrado!", HttpStatus.BAD_REQUEST);
        } else /*if(cliente.getVeiculos().size() != 0){
            return new ResponseEntity<>("Cliente tem veículos cadatrados!", HttpStatus.BAD_REQUEST);
        } else*/ {
            veiculoService.deleteById(id);
            return new ResponseEntity<>("Veículo removido com sucesso!", HttpStatus.OK);
        }
    }
}
