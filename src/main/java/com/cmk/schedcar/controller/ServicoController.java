package com.cmk.schedcar.controller;

import com.cmk.schedcar.model.Servico;
import com.cmk.schedcar.service.ServicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/servico")
public class ServicoController {
    
    @Autowired
    private ServicoService servicoService;

    @PostMapping()
    @ResponseBody
    private Servico salvar(@RequestBody Servico servico){
        servicoService.save(servico);
        return servico;
    }

    @GetMapping()
    @ResponseBody
    private List<Servico> buscar(){
        List<Servico> servicos = new ArrayList<>();
        servicoService.findAll().forEach(servicos::add);
        return servicos;
    }

    @PutMapping("/atualizar")
    @ResponseBody
    private Servico atualizar(@RequestBody Servico servico){
        servicoService.save(servico);
        return servico;
    }

    @DeleteMapping("/remover/{id}")
    private ResponseEntity<String> remover(@PathVariable Long id){
        Servico servico = servicoService.findById(id).orElse(null);
        if (Objects.isNull(servico)){
            return new ResponseEntity<>("Serviço não encontrado!", HttpStatus.BAD_REQUEST);
        } else /*if(servico.getVeiculos().size() != 0){
            return new ResponseEntity<>("servico tem veículos cadatrados!", HttpStatus.BAD_REQUEST);
        } else*/ {
            servicoService.deleteById(id);
            return new ResponseEntity<>("Serviço removido com sucesso!", HttpStatus.OK);
        }


    }
}
