package com.cmk.schedcar.controller;

import com.cmk.schedcar.model.Cliente;
import com.cmk.schedcar.service.ClienteService;
import com.cmk.schedcar.service.VeiculoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private VeiculoService veiculoService;

    @PostMapping()
    @ResponseBody
    private Cliente salvar(@RequestBody Cliente cliente){
        clienteService.save(cliente);
        return cliente;
    }

    @GetMapping()
    @ResponseBody
    private List<Cliente> buscar(){
        List<Cliente> clientes = new ArrayList<>();
        clienteService.findAll().forEach(clientes::add);
        return clientes;
    }

    @PutMapping("/atualizar")
    @ResponseBody
    private Cliente atualizar(@RequestBody Cliente cliente){
        clienteService.save(cliente);
        return cliente;
    }

    @DeleteMapping("/remover/{id}")
    private ResponseEntity<String> remover(@PathVariable Long id){
        Cliente cliente = clienteService.findById(id).orElse(null);
        if (Objects.isNull(cliente)){
            return new ResponseEntity<>("Cliente não encontrado!", HttpStatus.BAD_REQUEST);
        } else if(veiculoService.findByIdProprietario(cliente.getId()).size() != 0){
            return new ResponseEntity<>("Cliente tem veículos cadatrados!", HttpStatus.BAD_REQUEST);
        } else {
            clienteService.deleteById(id);
            return new ResponseEntity<>("Cliente removido com sucesso!", HttpStatus.OK);
        }


    }


}
