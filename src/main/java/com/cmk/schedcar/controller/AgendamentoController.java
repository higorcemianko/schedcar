package com.cmk.schedcar.controller;

import com.cmk.schedcar.dto.AgendamentoDTO;
import com.cmk.schedcar.model.Agendamento;
import com.cmk.schedcar.service.AgendamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/agendamento")
public class AgendamentoController {

    @Autowired
    private AgendamentoService agendamentoService;

    @PostMapping
    @ResponseBody
    private ResponseEntity<Agendamento> salvar(@RequestBody AgendamentoDTO agendamentoDTO){
        try{
            Agendamento agendamento = agendamentoDTO.toEntity();
            agendamento.setCancelado(false);
            agendamentoService.save(agendamento);
            agendamento.setValorTotal(agendamentoService.getValorTotal(agendamento));
            return new ResponseEntity<>(agendamento, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping
    @ResponseBody
    private ResponseEntity<List<Agendamento>> buscar(){
        try{
            List<Agendamento> agendamentos = new ArrayList<>();
            agendamentoService.findAll().forEach(agendamento -> {
                agendamento.setValorTotal(agendamentoService.getValorTotal(agendamento));
                agendamentos.add(agendamento);
            });
            return new ResponseEntity<>(agendamentos, HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    @ResponseBody
    private Agendamento buscarPorId(@PathVariable Long id){
        return agendamentoService.findById(id).orElse(null);
    }

    @PutMapping("/atualizar")
    @ResponseBody
    private Agendamento atualizar(@RequestBody Agendamento agendamento){
        agendamento.setCancelado(false);
        agendamentoService.save(agendamento);
        return agendamento;
    }

    @PutMapping("/cancelar/{id}")
    private ResponseEntity<String> cancelar(@PathVariable Long id){
        Agendamento agendamento = agendamentoService.findById(id).orElse(null);
        if (Objects.isNull(agendamento)){
            return new ResponseEntity<>("Agendamento não encontrado!", HttpStatus.BAD_REQUEST);
        } else {
            agendamento.setCancelado(true);
            agendamentoService.save(agendamento);
            return new ResponseEntity<>("Agendamento cancelado com sucesso!", HttpStatus.OK);
        }
    }



}
