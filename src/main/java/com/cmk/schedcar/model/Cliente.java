package com.cmk.schedcar.model;

import lombok.Data;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "clientes")
@Data
@EnableAutoConfiguration
public class Cliente implements Serializable {

    private static final long serialVersionUID = -6262654594254858882L;

    private static final String SEQUENCE = "SEQ_CLIENTE";

    @Id
    @SequenceGenerator(name=SEQUENCE, sequenceName = SEQUENCE, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE)
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "documento")
    private String documento;

    @Column(name = "endereco")
    private String endereco;

    @Column(name = "telefone")
    private Long telefone;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return id.equals(cliente.id) &&
                nome.equals(cliente.nome) &&
                documento.equals(cliente.documento) &&
                Objects.equals(endereco, cliente.endereco) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, documento, endereco);
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", documento='" + documento + '\'' +
                ", endereco='" + endereco + '\''  +
                '}';
    }
}
