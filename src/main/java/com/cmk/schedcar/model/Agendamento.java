package com.cmk.schedcar.model;

import com.cmk.schedcar.dto.AgendamentoDTO;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "agendamentos")
@Data
@NoArgsConstructor
public class Agendamento implements Serializable {

    private static final long serialVersionUID = 7245897160616855164L;

    private static final String SEQUENCE = "SEQ_AGENDAMENTO";

    @Id
    @SequenceGenerator(name=SEQUENCE, sequenceName = SEQUENCE, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE)
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "data_hora")
    private LocalDateTime dataHora;

    @Column(name = "tempo")
    private Integer tempo;

    @Column(name = "observacao")
    private String observacao;

    @Column(name = "id_veiculo")
    private Long idVeiculo;

    @ManyToOne
    @JoinColumn(name = "id_veiculo", insertable = false, updatable = false)
    private Veiculo veiculo;

    @Column(name = "cancelado")
    private Boolean cancelado;

    @Transient
    private BigDecimal valorTotal;

    @OneToMany(mappedBy = "idAgendamento")
    private List<AgendamentoServico> servicos;

    public Agendamento(LocalDateTime dataHora, Integer tempo, String observacao, Long idVeiculo, List<AgendamentoServico> servicos) {
        this.dataHora = dataHora;
        this.tempo = tempo;
        this.observacao = observacao;
        this.idVeiculo = idVeiculo;
        this.servicos = servicos;
    }






}
