package com.cmk.schedcar.model;

import lombok.Cleanup;
import lombok.Data;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "servicos")
@Data
@EnableAutoConfiguration
public class Servico implements Serializable {

    private static final long serialVersionUID = 4861440670641042440L;

    private static final String SEQUENCE = "SEQ_SERVICO";

    @Id
    @SequenceGenerator(name=SEQUENCE, sequenceName = SEQUENCE, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE)
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "valorUnidade")
    private BigDecimal valorUnidade;
}
