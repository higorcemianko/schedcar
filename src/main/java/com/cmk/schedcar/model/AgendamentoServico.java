package com.cmk.schedcar.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "agendamento_servicos")
@Data
@EnableAutoConfiguration
@NoArgsConstructor
public class AgendamentoServico implements Serializable {

    private static final long serialVersionUID = 3516157113403193608L;

    private static final String SEQUENCE = "SEQ_AGENDAMENTO_SERVICO";

    @Id
    @SequenceGenerator(name=SEQUENCE, sequenceName = SEQUENCE, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE)
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "id_agendamento")
    private Long idAgendamento;

    @Column(name = "id_servico")
    private Long idServico;

    @Column(name = "quantidade")
    private BigDecimal quantidade;

    @Transient
    private Servico servico;

    public AgendamentoServico(Long idServico, BigDecimal quantidade) {
        this.idServico = idServico;
        this.quantidade = quantidade;
    }
}
