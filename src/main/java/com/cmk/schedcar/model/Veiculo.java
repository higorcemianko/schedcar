package com.cmk.schedcar.model;

import com.cmk.schedcar.dto.RetornoVeiculoDTO;
import com.cmk.schedcar.dto.VeiculoDTO;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "veiculos")
@Data
@EnableAutoConfiguration
@NoArgsConstructor
public class Veiculo implements Serializable {


    private static final long serialVersionUID = 6608093790277174240L;
    private static final String SEQUENCE = "SEQ_VEICULO";

    @Id
    @SequenceGenerator(name=SEQUENCE, sequenceName = SEQUENCE, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE)
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "placa")
    private String placa;

    @Column(name = "marca")
    private String marca;

    @Column(name = "modelo")
    private String modelo;

    @Column(name = "ano")
    private Integer ano;

    @Column(name = "cor")
    private String cor;

    @Column(name = "id_proprietario")
    private Long idProprietario;

    @ManyToOne
    @JoinColumn(name = "id_proprietario", insertable = false, updatable = false)
    private Cliente proprietario;

    public Veiculo(String placa, String marca, String modelo, Integer ano, String cor, Long idProprietario) {
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.ano = ano;
        this.cor = cor;
        this.idProprietario = idProprietario;
    }

    public void setWithDTO(VeiculoDTO veiculoDTO){
        this.placa = veiculoDTO.getPlaca();
        this.marca = veiculoDTO.getMarca();
        this.modelo = veiculoDTO.getModelo();
        this.ano = veiculoDTO.getAno();
        this.cor = veiculoDTO.getCor();
        this.idProprietario = veiculoDTO.getIdProprietario();
    }


}
