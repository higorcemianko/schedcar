package com.cmk.schedcar.security.model;

import lombok.Data;

@Data
public class UsuarioCredencial {

    private String usuario;
    private String senha;
}
