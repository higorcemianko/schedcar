package com.cmk.schedcar.dto;

import com.cmk.schedcar.model.Agendamento;
import com.cmk.schedcar.model.AgendamentoServico;
import com.cmk.schedcar.model.Servico;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Column;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceConfigurationError;

@Data
@AllArgsConstructor
public class AgendamentoDTO {

    private String data;

    private String hora;

    private Integer tempo;

    private String observacao;

    private Long idVeiculo;

    private List<AgendamentoServicoDTO> servicos;

    private LocalDateTime getDateTime(String data, String hora){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        String dateTime = data + " " + hora;
        return LocalDateTime.parse(dateTime, formatter);
    }

    private List<AgendamentoServico> getAgendamentoServico(List<AgendamentoServicoDTO> servicosAgendar){
        List<AgendamentoServico> agendamentoServicos = new ArrayList<>();
        servicosAgendar.forEach(servicoAgendar -> agendamentoServicos.add(new AgendamentoServico(servicoAgendar.getIdServico(), servicoAgendar.getQuantidade())) );
        return agendamentoServicos;
    }

    public Agendamento toEntity(){
        return new Agendamento(getDateTime(this.data, this.hora), this.tempo, this.observacao, this.idVeiculo, getAgendamentoServico(this.servicos));
    }


}
