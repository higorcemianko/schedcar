package com.cmk.schedcar.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class AgendamentoServicoDTO {

    private Long idServico;
    private BigDecimal quantidade;
}
