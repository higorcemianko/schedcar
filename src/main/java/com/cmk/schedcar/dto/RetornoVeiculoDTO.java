package com.cmk.schedcar.dto;

import com.cmk.schedcar.model.Cliente;
import com.cmk.schedcar.model.Veiculo;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RetornoVeiculoDTO {
    private Long id;

    private String placa;

    private String marca;

    private String modelo;

    private Integer ano;

    private String cor;

    private Cliente proprietario;

    public static RetornoVeiculoDTO toRetornoDTO(Veiculo veiculo){
        return new RetornoVeiculoDTO(veiculo.getId(), veiculo.getPlaca(), veiculo.getMarca(), veiculo.getModelo(), veiculo.getAno(), veiculo.getCor(), veiculo.getProprietario());
    }
}
