package com.cmk.schedcar.dto;

import com.cmk.schedcar.model.Veiculo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
public class VeiculoDTO {

    private String placa;

    private String marca;

    private String modelo;

    private Integer ano;

    private String cor;

    private Long idProprietario;

    public Veiculo toEntity(){
        return new Veiculo(this.placa, this.marca, this.modelo, this.ano, this.cor, this.idProprietario );
    }


}
