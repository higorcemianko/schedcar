package com.cmk.schedcar.dao;

import com.cmk.schedcar.model.Veiculo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VeiculoDao extends CrudRepository<Veiculo, Long> {
    List<Veiculo> findByIdProprietario(Long idProprietario);

}
