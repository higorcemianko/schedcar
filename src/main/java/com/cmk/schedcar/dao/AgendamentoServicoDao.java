package com.cmk.schedcar.dao;

import com.cmk.schedcar.model.AgendamentoServico;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

public interface AgendamentoServicoDao extends CrudRepository<AgendamentoServico, Long> {

}
