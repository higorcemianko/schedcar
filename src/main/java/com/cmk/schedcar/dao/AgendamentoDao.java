package com.cmk.schedcar.dao;

import com.cmk.schedcar.model.Agendamento;
import org.springframework.data.repository.CrudRepository;

public interface AgendamentoDao extends CrudRepository<Agendamento, Long> {

}
