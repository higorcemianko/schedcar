package com.cmk.schedcar.dao;

import com.cmk.schedcar.model.Servico;
import org.springframework.data.repository.CrudRepository;

public interface ServicoDao extends CrudRepository<Servico, Long> {
}
