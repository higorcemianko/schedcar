package com.cmk.schedcar.dao;

import com.cmk.schedcar.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteDao extends CrudRepository<Cliente, Long> {

}
