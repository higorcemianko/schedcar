package com.cmk.schedcar.service;

import com.cmk.schedcar.dao.VeiculoDao;
import com.cmk.schedcar.dto.VeiculoDTO;
import com.cmk.schedcar.model.Veiculo;

import java.util.List;

public interface VeiculoService extends GenericService<Veiculo, Long>{

    public List<Veiculo> findByIdProprietario(Long idProprietario);

}
