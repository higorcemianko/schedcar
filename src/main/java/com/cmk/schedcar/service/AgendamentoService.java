package com.cmk.schedcar.service;

import com.cmk.schedcar.model.Agendamento;

import java.math.BigDecimal;

public interface AgendamentoService extends GenericService<Agendamento, Long> {

    public BigDecimal getValorTotal(Agendamento agendamento);

}
