package com.cmk.schedcar.service;

import com.cmk.schedcar.model.Cliente;

public interface ClienteService extends GenericService<Cliente, Long>{

}
