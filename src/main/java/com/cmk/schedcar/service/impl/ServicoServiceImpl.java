package com.cmk.schedcar.service.impl;

import com.cmk.schedcar.dao.ServicoDao;
import com.cmk.schedcar.model.Servico;
import com.cmk.schedcar.service.ServicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class ServicoServiceImpl extends GenericServiceImpl<Servico, Long> implements ServicoService {

    @Autowired
    private ServicoDao servicoDao;

    @Override
    protected CrudRepository<Servico, Long> getDao() {
        return servicoDao;
    }
}
