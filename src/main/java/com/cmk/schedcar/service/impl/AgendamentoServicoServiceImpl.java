package com.cmk.schedcar.service.impl;

import com.cmk.schedcar.dao.AgendamentoServicoDao;
import com.cmk.schedcar.model.AgendamentoServico;
import com.cmk.schedcar.service.AgendamentoServicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AgendamentoServicoServiceImpl extends GenericServiceImpl<AgendamentoServico, Long> implements AgendamentoServicoService {

    @Autowired
    private AgendamentoServicoDao agendamentoServicoDao;

    @Override
    protected CrudRepository<AgendamentoServico, Long> getDao() {
        return agendamentoServicoDao;
    }
}
