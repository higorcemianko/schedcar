package com.cmk.schedcar.service.impl;

import com.cmk.schedcar.dao.ClienteDao;
import com.cmk.schedcar.model.Cliente;
import com.cmk.schedcar.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class ClienteServiceImpl extends GenericServiceImpl<Cliente, Long> implements ClienteService {

    @Autowired
    private ClienteDao clienteDao;

    @Override
    protected CrudRepository<Cliente, Long> getDao() {
        return clienteDao;
    }
}
