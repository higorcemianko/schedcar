package com.cmk.schedcar.service.impl;

import com.cmk.schedcar.dao.AgendamentoDao;
import com.cmk.schedcar.model.Agendamento;
import com.cmk.schedcar.model.AgendamentoServico;
import com.cmk.schedcar.model.Servico;
import com.cmk.schedcar.service.AgendamentoService;
import com.cmk.schedcar.service.AgendamentoServicoService;
import com.cmk.schedcar.service.ServicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Optional;

@Service
@Transactional
public class AgendamentoServiceImpl extends GenericServiceImpl<Agendamento, Long> implements AgendamentoService {

    @Autowired
    private AgendamentoDao agendamentoDao;

    @Autowired
    private AgendamentoServicoService agendamentoServicoService;

    @Autowired
    private ServicoService servicoService;

    @Override
    protected CrudRepository<Agendamento, Long> getDao() {
        return agendamentoDao;
    }

    @Override
    public void save(Agendamento agendamento) {
        agendamentoDao.save(agendamento);
        salvarServicos(agendamento);
    }

    private void salvarServicos(Agendamento agendamento) {
        agendamento.getServicos().forEach(servico -> {
            servico.setIdAgendamento(agendamento.getId());
            agendamentoServicoService.save(servico);
        });
    }

    @Override
    public BigDecimal getValorTotal(Agendamento agendamento)  {

        BigDecimal valorTotal = BigDecimal.ZERO;
        for(AgendamentoServico agendamentoServico : agendamento.getServicos()){
            Optional<Servico> opServico = servicoService.findById(agendamentoServico.getIdServico());
            if (opServico.isPresent()){
                agendamentoServico.setServico(opServico.get());
                valorTotal = valorTotal.add(agendamentoServico.getQuantidade().multiply(agendamentoServico.getServico().getValorUnidade()));
            }
        }
        return valorTotal;

    }
}
