package com.cmk.schedcar.service.impl;

import com.cmk.schedcar.dao.VeiculoDao;
import com.cmk.schedcar.model.Veiculo;
import com.cmk.schedcar.service.VeiculoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

@Service
@Transactional
public class VeiculoServiceImpl extends GenericServiceImpl<Veiculo, Long> implements VeiculoService {

    @Autowired
    private VeiculoDao veiculoDao;

    @Override
    protected CrudRepository<Veiculo, Long> getDao() {
        return veiculoDao;
    }


    @Override
    public List<Veiculo> findByIdProprietario(Long idProprietario) {
        return  veiculoDao.findByIdProprietario(idProprietario);
    }
}
