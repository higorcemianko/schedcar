package com.cmk.schedcar.service.impl;

import com.cmk.schedcar.service.GenericService;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public abstract class GenericServiceImpl<T, ID> implements GenericService<T, ID> {

    protected abstract CrudRepository<T, ID> getDao();

    @Override
    public void save(T var1) {
        getDao().save(var1);
    }

    @Override
    public Optional<T> findById(ID var1) {
        return getDao().findById(var1);
    }

    @Override
    public boolean existsById(ID var1) {
        return getDao().existsById(var1);
    }

    @Override
    public Iterable<T> findAll() {
        return getDao().findAll();
    }

    @Override
    public Iterable<T> findAllById(Iterable<ID> var1) {
        return getDao().findAllById(var1);
    }

    @Override
    public long count() {
        return getDao().count();
    }

    @Override
    public void deleteById(ID var1) {
        getDao().deleteById(var1);
    }

    @Override
    public void delete(T var1) {
        getDao().delete(var1);
    }

    @Override
    public void deleteAll(Iterable<? extends T> var1) {
        getDao().deleteAll(var1);
    }

    @Override
    public void deleteAll() {
        getDao().deleteAll();
    }
}
